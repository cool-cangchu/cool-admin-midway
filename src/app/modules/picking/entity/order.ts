import { EntityModel } from '@midwayjs/orm';
import { BaseEntity } from 'midwayjs-cool-core';
import { Column, Index, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { PickWareHourseEntity } from './warehouse';

/**
 * 订单号
 */
@EntityModel('pick_order')
export class OrderEntity extends BaseEntity {
  @Index({ unique: true })
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ comment: '库位名称', nullable: true, length: 50 })
  name: string;

  @Column({ comment: '备注', default: '' })
  note: string;

  @Column({ comment: '父级', nullable: true, default: 0 })
  pid: number;

  @Column({
    comment: '创建时间',
    type: 'date',
    default: () => 'CURRENT_DATE',
    precision: 6,
  })
  createTime: Date;

  @Column({
    comment: '更新时间',
    type: 'date',
    default: () => 'CURRENT_DATE',
    precision: 6,
  })
  updateTime: Date;

  @OneToMany(type => PickWareHourseEntity, sort => sort.orderId) // note: we will create author property in the Photo class below
  wareorder: PickWareHourseEntity[];
}
