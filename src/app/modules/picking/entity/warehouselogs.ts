import { EntityModel } from '@midwayjs/orm';
import { BaseEntity } from 'midwayjs-cool-core';
import { Column, Index, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { OrderEntity } from './order';

/**
 * 捡单
 */
@EntityModel('pick_warehouse_log')
export class PickWareHourseLogEntity extends BaseEntity {
  @Index()
  @Column({ select: false })
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ comment: '资产名称', nullable: true })
  orderName: string;

  @Column({ comment: '库位名称', nullable: true })
  kuweiName: string;

  @Column({ comment: '订单id', nullable: true, default: 0 })
  orderId: string;

  @Column({ comment: '资产编码', nullable: true })
  barCode: string;

  @Column({ comment: '型号规格', nullable: true })
  specifications: string;

  @Column({ comment: '需求数量', nullable: true, default: 0 })
  quantityNum: number;

  @Column({ comment: '实际扫码数量', nullable: true, default: 0 })
  realNum: number;

  @Column({
    comment: '创建时间',
    type: 'datetime',
    default: () => 'CURRENT_DATETIME',
    precision: 6,
  })
  createTime: Date;

  @ManyToOne(type => OrderEntity, order => order.id)
  order: OrderEntity;
}
