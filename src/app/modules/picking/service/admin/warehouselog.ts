import { Provide, Inject } from '@midwayjs/decorator';
import { BaseService } from 'midwayjs-cool-core';
import { InjectEntityModel } from '@midwayjs/orm';
import { Repository } from 'typeorm';
import { Context } from 'egg';
import { OrderEntity } from '../../entity/order';
import { PickWareHourseLogEntity } from '../../entity/warehouselogs';
import * as moment from 'moment';

/**
 * 拣货单
 */
@Provide()
export class WarehouseLogsService extends BaseService {
  @InjectEntityModel(OrderEntity)
  orderEntity: Repository<OrderEntity>; // 订单号

  @InjectEntityModel(PickWareHourseLogEntity)
  PickWareHourseLogEntity: Repository<PickWareHourseLogEntity>; // 备注

  // @Inject()
  // WarehouseLogsService: WarehouseLogsService; // 捡货单

  @Inject()
  ctx: Context;

  async list() {
    const where = [];
    let whereSql = '';
    const body = this.ctx.request.body;
    if (body.barCode) {
      where.push(`barCode = "${body.barCode}"`);
    }
    if (body.time) {
      const time = moment(body.time).format('YYYY-MM-DD');
      where.push(`to_days(updateTime) = to_days("${time}")`);
    }
    console.log(body.time);
    console.log(where, '>>>>>>>>>>>>>>>>');
    if (where.length) {
      whereSql = `where ${where.join(' AND ')}`;
    }
    const select = `select * from pick_warehouse_log ${whereSql}`;
    const item = await this.nativeQuery(select);
    return item;
  }
}
