import { Provide } from '@midwayjs/decorator';
import { CoolController, BaseController } from 'midwayjs-cool-core';
import { PickWareHourseEntity } from '../../entity/warehouse';
import { OrderEntity } from '../../entity/order';
import { Context } from 'egg';
import { WarehouseService } from '../../service/admin/warehouse';

/**
 * 捡单
 */
@Provide()
@CoolController({
  api: ['add', 'update', 'delete', 'info', 'list', 'page'],
  pageQueryOp: {
    keyWordLikeFields: ['a.barCode', 'a.warname'],
    select: ['a.*', 'b.name'],
    fieldEq: ['a.barCode'],
    addOrderBy: {
      createTime: 'asc',
    },
    leftJoin: [
      {
        entity: OrderEntity,
        alias: 'b',
        condition: 'a.orderId = b.id',
      },
    ],
    where: async (ctx: Context) => {
      const body = ctx.request.body;
      const orderId = [];
      if (body.orderId > 0) {
        orderId.push(['b.id = :orderId', { orderId: body.orderId }]);
      }
      if (body.barCode > 0) {
        orderId.push(['a.barCode = :barCode', { barCode: body.barCode }]);
      }
      return orderId;
    },
  },
  entity: PickWareHourseEntity,
  service: WarehouseService,
})
export class PickWarehouseapiController extends BaseController {}
