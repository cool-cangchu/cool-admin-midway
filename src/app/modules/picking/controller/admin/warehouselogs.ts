import { Provide } from '@midwayjs/decorator';
import { CoolController, BaseController } from 'midwayjs-cool-core';
import { PickWareHourseLogEntity } from '../../entity/warehouselogs';
import { Context } from 'egg';
import { WarehouseLogsService } from '../../service/admin/warehouselog';
import * as moment from 'moment';
/**
 * 捡单
 */
@Provide()
@CoolController({
  api: ['list', 'page'],
  pageQueryOp: {
    keyWordLikeFields: ['barCode', 'orderName'],
    select: ['*'],
    addOrderBy: {
      createTime: 'asc',
    },
    where: async (ctx: Context) => {
      const body = ctx.request.body;
      const orderId = [];
      if (body.time) {
        const time = moment(body.time).format('YYYY-MM-DD');
        orderId.push(['to_days(updateTime) = to_days(:time)', { time: time }]);
      }
      if (body.barCode) {
        orderId.push(['barCode = :barCode', { barCode: body.barCode }]);
      }
      return orderId;
    },
  },
  entity: PickWareHourseLogEntity,
  service: WarehouseLogsService,
})
export class PickWarehouseLogsController extends BaseController {}
