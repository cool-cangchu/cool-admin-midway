import { Provide, Inject } from '@midwayjs/decorator';
import { Context } from 'egg';
import { CoolController, BaseController } from 'midwayjs-cool-core';
import { OrderEntity } from '../../entity/order';
import { OrderService } from '../../service/admin/order';

/**
 * 订单号
 */
@Provide()
@CoolController({
  api: ['list', 'page', 'update', 'info', 'add'],
  pageQueryOp: {
    keyWordLikeFields: ['a.name'],
    select: ['a.*', 'b.name as pName'],
    addOrderBy: {
      id: 'asc',
    },
    leftJoin: [
      {
        entity: OrderEntity,
        alias: 'b',
        condition: 'a.pid=b.pid',
      },
    ],
    // 增加其他条件
    where: async ctx => {
      const body = ctx.request.body;
      const time = [];
      if (body.id && body.id > 0) {
        time.push(['id = :id', { id: body.id }]);
      }
      return time;
    },
  },
  entity: OrderEntity,
  service: OrderService,
})
export class OrderApiController extends BaseController {
  @Inject()
  ctx: Context;
  @Inject()
  OrderService: OrderService;
}
