import { Application } from 'egg';
import { ModuleConfig } from 'midwayjs-cool-core';

/**
 * 模块的配置
 */
export default (app: Application) => {
  return {
    // 模块名称
    name: '物流管理系统',
    // 模块描述
    description: '物流管理系统',
    // 中间件
    middlewares: ['baseAuthorityMiddleware', 'baseLogMiddleware'],
  } as ModuleConfig;
};
